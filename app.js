const path = require('path');
const fs = require('fs');
const url = require('url');
const file_management = require("../arkiweraengine/app/file.management");
const { Arkiwera_Engine, WebFile } = require('../arkiweraengine/app/arkiwera.engine.js');
const { sleep } = require("../arkiweraengine/app/sleep.js");
const { Op } = require("sequelize");


console.log('=================================');
console.log('=        Process starts....     =');
console.log('=================================');

// main code

let b_database = false;
(async () => {
    try {
        let config = {};
        if (fs.existsSync(__dirname + path.sep + 'config.json')) {
            const json = fs.readFileSync(__dirname + path.sep + 'config.json').toString();
            config = JSON.parse(json);
            config.root = path.resolve(__dirname + path.sep);
            config.path = path.resolve(__dirname + path.sep + 'config.json');
            // file_management.saveJsonObjectToFile(config.path, config);
        } else {
            throw 'Saknar ' + __dirname + path.sep + 'config.json';
        }
        /**
         * Create Database if the databasename isn't exists.
         * OMR can't create database
         */
        console.log('\r\nCreate/Initialize Database...');
        const DB = require('..' + path.sep + 'arkiweraengine' + path.sep + 'app' + path.sep + 'db');
        const con = new DB(config.mysql);
        b_database = await con.initialize(config.mysql)
        if (b_database) {
            console.log('Databasen är redo.');
        } else {
            console.log('Databasen är inte redo. Exit at once!');
            process.exit();
        }

        /**
         * Create ORM object
         */
        console.log('\r\nCreate/Initialize ORM Object ...');
        const ORM = require('..' + path.sep + 'arkiweraengine' + path.sep + 'app' + path.sep + 'orm');
        let orm = new ORM(config.mysql);
        b_databse = await orm.initialize(config.mysql);
        if (b_database) {
            console.log('ORM är redo.');
        } else {
            console.log('ORM är inte redo. Exit at once!');
            process.exit();
        }


        /**
         * open Puppeteer.
         */
        if (config.domain.save_directory == undefined) {
            config.domain.save_directory = __dirname + path.sep + "downloads" + path.sep;
        } else {
            config.domain.save_directory = path.resolve(__dirname + path.sep + config.domain.save_directory) + path.sep; //säkerställa att save_directory slutar på "/"
        }

        const args = require('minimist')(process.argv.slice(2))
        let is_need_convert = true;
        let is_need_download = true;
        console.log("\r\nparameter:", args);
        if (args['action'] != undefined) {
            const actions = args['action'].toLowerCase().split('|');
            for (let index = 0; index < actions.length; index++) {
                const element = actions[index];
                if (element == 'noconvert') {
                    is_need_convert = false;
                }
                if (element == 'nodownload') {
                    is_need_download = false;
                }

            }
        }
        console.log("need to convert:", is_need_convert);
        console.log("need to download:", is_need_download);
        const arkiwera_engine = new Instance(config, orm);
        await arkiwera_engine.initialize_Engine();

        if (is_need_download) {
            console.log('start download...');
            await arkiwera_engine.download();
        }
        if (is_need_convert) {
            console.log('start convert...');
            await arkiwera_engine.convert();
        }
        // console.log(result);
        await arkiwera_engine.dispose();
        /**
         * close ORM
         */
        await orm.close();

    } catch (err) {
        console.log(err);
    } finally {
        console.log('=================================');
        console.log('=        Process ends ....      =');
        console.log('=================================');
        process.exit();
    }
})();


class Instance extends Arkiwera_Engine {
    constructor(config, orm, login = null) {
        super(config, orm, login);
    }
    /**
         * judge if it need to log in agian.
         * Sometimes the login is only valid for one minute
         * @param {*} page 
         * @returns 1: url is valid; 2: need to log in agian and login has become valid again; 3: login is invalid
         */
    async double_check_login(pagetype, url) {
        let rtn = 1;
        const redirect_login = "https://heby.se/intranat/wp-login.php?redirect";
        let redirect_url = '';
        if (pagetype == 'main') {
            redirect_url = this.mainPage.url();
        } else {
            redirect_url = this.attachmentPage.url();
        }

        let indexOfFirst = redirect_url.indexOf(redirect_login);
        if (indexOfFirst == 0) {
            // need to log in again
            try {
                await this.login2(this.defaultPageUrl, this.username, this.password, pagetype);
                if (pagetype == 'main') {
                    await this.mainPage.goto(url, { waitUntil: this.config.session.puppeteer.waitUntil });
                    redirect_url = this.mainPage.url();
                } else {
                    await this.attachmentPage.goto(url, { waitUntil: this.config.session.puppeteer.waitUntil });
                    redirect_url = this.attachmentPage.url();
                }
                indexOfFirst = redirect_url.indexOf(redirect_login);
                if (indexOfFirst == 0) {
                    rtn = 3;
                } else {
                    rtn = 2;
                }
            }
            catch (err) {
                console.log(err);
                rtn = 3;
            }

        }
        return rtn;
    }
    do_extrawork_with_objlink(objlink) {
        const rtn = super.do_extrawork_with_objlink(objlink);
        if (rtn.originalurl == undefined) {
            return rtn;
        }
        //clear ?1639749190.pdf
        const f_index = rtn.originalurl.indexOf('?');
        if (f_index > 0) {
            // det finns ? tecken.
            const f_string = rtn.originalurl.substring(f_index);
            let reg = new RegExp(/\?[0-9]+\.[a-zA-Z]*/i);
            if (reg.test(f_string)) {
                // clear ?*****
                rtn.originalurl = rtn.originalurl.substring(0, f_index);
                rtn.absoluteurl = rtn.absoluteurl.replace(f_string, '');
                rtn.url = rtn.url.replace(f_string, '');
            }

        };

        const c = rtn.originalurl.substring(0, 1);
        const valid_char = ['#', '?'];
        if (valid_char.includes(c)) {
            //clear ? parameter
            rtn.url = rtn.url.replace(rtn.originalurl, '');
            rtn.absoluteurl = rtn.url.replace(rtn.originalurl, '');
            rtn.originalurl = '';
            rtn.is_internal_url = this.check_Internal_domain(rtn.absoluteurl);
            rtn.valid = rtn.is_internal_url;
        }
        return rtn;
    }
    /**
     * A = https://heby.se/intranat/2021/10/29/nu-finns-det-barnhalsoteam-i-heby-kommun/ 
     * A har link ?1639665129.
     * A?1639665129. är egentiligen samma som A
     * A?1639665129. ska ha ett nytt nummer ?1639666336.
     * A?1639666336. är också egentiligen samma som A
     * Det innebär att den här sidan kan leda till obegränsad loop.
     * så hantera inte ?link 
     * 
     * @param {*} href 
     * @returns 
     */
    is_valid_url(href) {
        let rtn = super.is_valid_url(href);
        if (rtn) {
            const c = href.substring(0, 1);
            if ("?" == c) {
                return false;
            }
        }
        return rtn;

    }
    is_blacklist(url) {
        const blacklist = [
            "https://heby.se/intranat/wp-login.php?action=logout",
            "https://heby.se/dokumentarkiv/wp-admin",
        ];
        let rtn = false;
        for (let prefix of blacklist) {
            const indexOfFirst = url.indexOf(prefix);
            if (indexOfFirst == 0) {
                //this is logut, don't download.
                rtn = true;
                break;
            }
        }

        return rtn;
    }
    async login2(defaultUrl, userName, password, type = 'main') {
        let rtn = false;
        try {

            const sleeptime = 1000;
            if (type == 'main') {
                console.log(await this.mainPage.title());
            } else {
                console.log(await this.attachmentPage.title());
            }
            await sleep(sleeptime);
            /**
             * innan username måste man sova en liten stund annars Microsoft returnerar alltid en fel sida
             */
            if (type == 'main') {
                await this.mainPage.type('#user_login', userName);
                await sleep(sleeptime);
                await this.mainPage.type('#user_pass', password);
                const element = await this.mainPage.$('input[id="rememberme"]');
                if (element == undefined) {
                    return false;
                }
                await sleep(sleeptime);
                await this.mainPage.click('input[id="rememberme"]');
                await sleep(sleeptime);
                await this.mainPage.click('[id="wp-submit"]');

            } else {
                await this.attachmentPage.type('#user_login', userName);
                await sleep(sleeptime);
                await this.attachmentPage.type('#user_pass', password);
                const element = await this.attachmentPage.$('input[id="rememberme"]');
                if (element == undefined) {
                    return false;
                }
                await sleep(sleeptime);
                await this.attachmentPage.click('input[id="rememberme"]');
                await sleep(sleeptime);
                await this.attachmentPage.click('[id="wp-submit"]');

            }
            await sleep(sleeptime);
            let title = '';
            if (type == 'main') {
                title = await this.mainPage.title();
            } else {
                title = await this.attachmentPage.title();
            }
            console.log(title);
            if (title == 'Logga in ‹ Intranät — WordPress') {
                console.log("Failed to log in! ");
                return false;
            }
            rtn = true;
        } catch (err) {
            console.log(err);
            rtn = false;
        }
        finally {
            return rtn;
        }

    }
    async login(defaultUrl, userName, password) {
        let rtn = false;
        try {
            await this.mainPage.goto('https://heby.se/intranat/wp-login.php', { waitUntil: this.config.session.puppeteer.waitUntil });
            const sleeptime = 1000;
            console.log(await this.mainPage.title());
            await sleep(sleeptime);
            /**
             * innan username måste man sova en liten stund annars Microsoft returnerar alltid en fel sida
             */
            await this.mainPage.type('#user_login', userName);
            await sleep(sleeptime);
            await this.mainPage.type('#user_pass', password);
            const element = await this.mainPage.$('input[id="rememberme"]');
            if (element == undefined) {
                return false;
            }
            await this.mainPage.click('input[id="rememberme"]');
            await sleep(sleeptime);
            await this.mainPage.click('[id="wp-submit"]');
            await sleep(sleeptime);

            const title = await this.mainPage.title();
            console.log(title);
            if (title == 'Logga in ‹ Intranät — WordPress') {
                console.log("Failed to log in! ");
                return false;
            }
            rtn = true;
        } catch (err) {
            console.log(err);
            rtn = false;
        }
        finally {
            return rtn;
        }

    }

    /**
     * skriva över den här funktionen.
     * den första sidan behöver några musklick för att hämta mer nyheter och sedan får man spara hela sidan.
     * @param {} PuppeteerPage 
     * @returns 
     */
    async execute_extra(PuppeteerPage, checkUrl) {


    }
    // letar efter <script> windows.__Model ="***" </scirpt>
    async convert_special(fileName) {
        // delete <base href="/">

        return true;
    }
} // end of class Instance